package hu.bereczki.spring.gateway.demo.routes;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import hu.bereczki.spring.gateway.demo.GatewayRoute;

public class ApplicationCreate extends GatewayRoute {

    public ApplicationCreate(String elasticUrl) {
        super(elasticUrl);
    }

    @Override
    public Route.AsyncBuilder buildRoute(PredicateSpec spec) {
        return spec.method(HttpMethod.POST)
                .and()
                .path("/applications/application")
                .filters(filter -> filter.rewritePath("/applications/application", "/applications/_doc")
                        .modifyResponseBody(String.class, String.class, MediaType.APPLICATION_JSON_VALUE,
                                this::modifyStatusInResponse))
                .uri(elasticUrl);
    }
}
