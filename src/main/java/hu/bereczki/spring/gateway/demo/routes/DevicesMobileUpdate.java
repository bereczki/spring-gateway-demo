package hu.bereczki.spring.gateway.demo.routes;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.http.HttpMethod;

import hu.bereczki.spring.gateway.demo.GatewayRoute;

public class DevicesMobileUpdate extends GatewayRoute {

    public DevicesMobileUpdate(String elasticUrl) {
        super(elasticUrl);
    }

    @Override
    public Route.AsyncBuilder buildRoute(PredicateSpec spec) {
        return spec.method(HttpMethod.POST)
                .and()
                .path("/devices/mobileDevice/*/_update")
                .filters(filter ->
                        filter.rewritePath("/devices/mobileDevice/(?<ID>.*)/_update", "/devices/_update/${ID}"))
                .uri(elasticUrl);
    }
}
