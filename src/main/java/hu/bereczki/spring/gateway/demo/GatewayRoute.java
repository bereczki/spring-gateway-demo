package hu.bereczki.spring.gateway.demo;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.web.server.ServerWebExchange;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import reactor.core.publisher.Mono;

public abstract class GatewayRoute {

    private static final String CREATED = "created";
    private static final String RESULT_FIELD = "result";
    private static final String ITEMS_FIELD = "items";
    private static final String INDEX_FIELD = "index";

    protected final String elasticUrl;

    protected final ObjectMapper objectMapper = new ObjectMapper();

    protected GatewayRoute(String elasticUrl) {
        this.elasticUrl = elasticUrl;
    }

    public abstract Route.AsyncBuilder buildRoute(PredicateSpec spec);

    protected Mono<String> modifyStatusInResponse(ServerWebExchange serverWebExchange, String responseString) {
        return Mono.fromCallable(() -> {
            final ObjectNode response = (ObjectNode) objectMapper.readTree(responseString);

            if (response.has(ITEMS_FIELD)) {
                response.get(ITEMS_FIELD).forEach(item -> {
                    final ObjectNode index = (ObjectNode) item.get(INDEX_FIELD);
                    if (index != null && CREATED.equalsIgnoreCase(index.get(RESULT_FIELD).asText())) {
                        response.put(CREATED, true);
                    }
                });
            }

            if (response.has(RESULT_FIELD) && CREATED.equalsIgnoreCase(response.get(RESULT_FIELD).asText())) {
                response.put(CREATED, true);
            }

            return response.toString();
        });
    }

}
