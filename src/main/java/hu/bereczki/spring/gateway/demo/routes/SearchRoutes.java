package hu.bereczki.spring.gateway.demo.routes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import hu.bereczki.spring.gateway.demo.GatewayRoute;
import reactor.core.publisher.Mono;

public abstract class SearchRoutes extends GatewayRoute {

    private static final String TOTAL_FIELD = "total";
    private static final String HITS_FIELD = "hits";
    private static final String VALUE_FIELD = "value";

    protected SearchRoutes(String elasticUrl) {
        super(elasticUrl);
    }

    protected Mono<String> modifySearchResponse(String responseString) {
        return Mono.fromCallable(() -> manipulateResult(responseString));
    }

    private String manipulateResult(String responseString) throws JsonProcessingException {
        final JsonNode response = objectMapper.readTree(responseString);
        final ObjectNode hits = (ObjectNode) response.get(HITS_FIELD);

        if (hits == null || !hits.has(TOTAL_FIELD)) {
            return responseString;
        }

        final JsonNode total = hits.get(TOTAL_FIELD);
        if (!total.isObject()) {
            return responseString;
        }

        hits.remove(TOTAL_FIELD);
        hits.put(TOTAL_FIELD, total.get(VALUE_FIELD).longValue());

        return response.toString();
    }
}
