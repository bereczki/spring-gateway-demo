package hu.bereczki.spring.gateway.demo.routes;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import reactor.core.publisher.Mono;

public class ApplicationSearch extends SearchRoutes {

    public ApplicationSearch(String elasticUrl) {
        super(elasticUrl);
    }

    @Override
    public Route.AsyncBuilder buildRoute(PredicateSpec spec) {
        return spec.method(HttpMethod.POST)
                .and()
                .path("/applications/_search")
                .filters(filter -> filter.modifyResponseBody(String.class, String.class,
                        MediaType.APPLICATION_JSON_VALUE,
                        (serverWebExchange, responseString) -> modifySearchResponse(responseString)))
                .uri(elasticUrl);
    }
}
