package hu.bereczki.spring.gateway.demo.routes;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import reactor.core.publisher.Mono;

public class DeviceSearch extends SearchRoutes {

    public DeviceSearch(String elasticUrl) {
        super(elasticUrl);
    }

    @Override
    public Route.AsyncBuilder buildRoute(PredicateSpec spec) {
        return spec.method(HttpMethod.POST)
                .and()
                .path("/devices/_search")
                .filters(filter -> filter.filter((exchange, chain) -> chain.filter(exchange.mutate()
                        .request(exchange.getRequest().mutate()
                                .method(HttpMethod.GET).build())
                        .build()), Integer.MIN_VALUE)
                        .modifyResponseBody(String.class, String.class,
                                MediaType.APPLICATION_JSON_VALUE,
                                (serverWebExchange, responseString) -> modifySearchResponse(responseString)))
                .uri(elasticUrl);
    }
}
