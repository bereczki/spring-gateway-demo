package hu.bereczki.spring.gateway.demo.routes;

import java.io.UncheckedIOException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import hu.bereczki.spring.gateway.demo.GatewayRoute;
import reactor.core.publisher.Mono;

public class Bulk extends GatewayRoute {

    public Bulk(String elasticUrl) {
        super(elasticUrl);
    }

    @Override
    public Route.AsyncBuilder buildRoute(PredicateSpec spec) {
        return spec.method(HttpMethod.POST)
                .and()
                .path("/_bulk")
                .filters(filter -> filter
                        .setRequestHeader(HttpHeaders.CONTENT_TYPE, "application/x-ndjson")
                        .modifyRequestBody(String.class, String.class, MediaType.APPLICATION_JSON_VALUE,
                                this::modifyBulkRequest)
                        .modifyResponseBody(String.class, String.class, MediaType.APPLICATION_JSON_VALUE,
                                this::modifyStatusInResponse))
                .uri(elasticUrl);
    }

    private Mono<String> modifyBulkRequest(ServerWebExchange serverWebExchange, String requestString) {
        return Mono.fromCallable(() -> requestString.lines()
                .map(s -> {
                    try {
                        return objectMapper.readTree(s);
                    } catch (JsonProcessingException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .map(request -> {
                    request.forEach(jsonNode -> {
                        if (jsonNode instanceof ObjectNode) {
                            ((ObjectNode) jsonNode).remove("_type");
                        }
                    });
                    return request;
                })
                .map(JsonNode::toString)
                .collect(Collectors.joining(System.lineSeparator())) + System.lineSeparator());
    }

}
