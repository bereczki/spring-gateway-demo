package hu.bereczki.spring.gateway.demo.routes;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.http.HttpMethod;

import hu.bereczki.spring.gateway.demo.GatewayRoute;

public class UpdateStatisticsUpdate extends GatewayRoute {

    public UpdateStatisticsUpdate(String elasticUrl) {
        super(elasticUrl);
    }

    @Override
    public Route.AsyncBuilder buildRoute(PredicateSpec spec) {
        return spec.method(HttpMethod.POST)
                .and()
                .path("/updatestatistics/statistic/{docId}/_update")
                .filters(filter -> filter
                        .rewritePath("/updatestatistics/statistic/(?<DOCID>.*)/_update", "/updatestatistics/_update/${DOCID}"))
                .uri(elasticUrl);
    }
}
