package hu.bereczki.spring.gateway.demo;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Set;

import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayDemoApplication {

// Eureka Service Discovery Client: (now there is no Eureka Server so not in use!)
//    @Bean
//    DiscoveryClientRouteDefinitionLocator discoveryRoutes(ReactiveDiscoveryClient dc, DiscoveryLocatorProperties dlp) {
//        return new DiscoveryClientRouteDefinitionLocator(dc, dlp);
//    }

    @Value("${elastic.url:http://192.168.100.69:9200}")
    private String elasticUrl;

    @Bean
    RouteLocator gatewayRoutes(RouteLocatorBuilder builder) throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {
        Reflections reflections = new Reflections("hu.bereczki.spring.gateway.demo");
        Set<Class<? extends GatewayRoute>> routeBuilders = reflections.getSubTypesOf(GatewayRoute.class);

        final RouteLocatorBuilder.Builder routes = builder.routes();

        for (Class<? extends GatewayRoute> routeClass : routeBuilders) {
            if (!Modifier.isAbstract(routeClass.getModifiers())) {
                final Constructor<? extends GatewayRoute> constructor = routeClass.getConstructor(String.class);
                final GatewayRoute route = constructor.newInstance(elasticUrl);
                routes.route(routeClass.getSimpleName(), route::buildRoute);
            }
        }

        return routes.build();
    }


    public static void main(String[] args) {
        SpringApplication.run(GatewayDemoApplication.class, args);
    }
}
