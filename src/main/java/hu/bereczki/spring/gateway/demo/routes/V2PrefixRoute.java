package hu.bereczki.spring.gateway.demo.routes;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;

import hu.bereczki.spring.gateway.demo.GatewayRoute;

public class V2PrefixRoute extends GatewayRoute {

    public V2PrefixRoute(String elasticUrl) {
        super(elasticUrl);
    }

    @Override
    public Route.AsyncBuilder buildRoute(PredicateSpec spec) {
        return spec.path("/v2/**")
                .filters(filterSpec -> filterSpec.rewritePath("/v2/(?<urlPath>.*)", "/${urlPath}"))
                .uri(elasticUrl);
    }
}
