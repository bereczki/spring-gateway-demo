package hu.bereczki.spring.gateway.demo.routes;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.http.HttpMethod;

import hu.bereczki.spring.gateway.demo.GatewayRoute;

public class ApplicationUpdate extends GatewayRoute {

    public ApplicationUpdate(String elasticUrl) {
        super(elasticUrl);
    }

    @Override
    public Route.AsyncBuilder buildRoute(PredicateSpec spec) {
        return spec.method(HttpMethod.POST)
                .and()
                .path("/applications/application/*/_update")
                .filters(filter -> filter
                        .rewritePath("/applications/application/(?<ID>.*)/_update", "/applications/_update/${ID}"))
                .uri(elasticUrl);
    }
}
